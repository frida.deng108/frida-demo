module.exports = {
  printWidth: 120, // 单行长度
  tabWidth: 2, // tab缩进大小,默认为2
  useTabs: false, // 使用空格替代tab缩进
  semi: true, // 行尾是否使用分号，默认为true
  trailingComma: 'es5', // 是否使用尾逗号，有三个可选值"<none|es5|all>"
  jsxBracketSameLine: false, // jsx标签闭合位置，默认false换行闭合
  arrowParens: 'avoid', // 箭头函数参数括号，默认avoid 可选 avoid| always 避免括号|始终有括号
  endOfLine: 'auto', // 换行符使用 lf|crlf|cr|auto auto: 换行符使用编辑器自动的换行格式 lf: 仅unix和linux使用的换行符 crlf: 仅windows使用的换行符 cr: 仅mac使用的换行符
  bracketSpacing: true, // 对象大括号直接是否有空格，默认为true，效果：{ foo: bar }
  singleQuote: true, // 字符串是否使用单引号，默认为false，使用双引号
};
