import axios, { AxiosRequestConfig, AxiosError, AxiosInstance, AxiosResponse } from 'axios';
// import AxiosRetry from 'axios-retry';

const baseConfig: Partial<AxiosRequestConfig> = {
  timeout: 10000,
  // baseURL: '/backend/api',
  // headers: {
  //   'Content-Type': 'application/json',
  // },
};

const client: AxiosInstance = axios.create(baseConfig);
// 错误自动重试，最多尝试3次
// AxiosRetry(client, { retries: 3 });

client.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    // 发送前的操作
    // ……
    config.headers.authToken ??= console.log('before request');
    console.log('config', config);
    return config;
  },
  (error: AxiosError) => {
    return Promise.reject(error);
  }
);

client.interceptors.response.use(
  (response: AxiosResponse) => {
    // 2XX 状态码触发执行
    return response.data;
  },
  (error: AxiosError) => {
    // 对错误响应的操作
    // ……
    const status = error.response?.status;
    switch (status) {
      case 400:
        console.log('跳转404页面');
        break;
      case 500:
        console.log('服务不可用');
        break;
    }
    return Promise.reject(error);
  }
);

// const http = async <T>(
//   method: AxiosRequestConfig['method'],
//   url: string,
//   config?: AxiosRequestConfig
// ): Promise<ResponseData<T> | null> => {
//   try {
//     const res = await client(url, { method, ...(config || {}) });
//     console.log('after request');
//     if (res?.data?.status !== 'success') {
//       // TODO: 存在错误，弹窗提示
//       console.error('httpService:', res.data.message);
//     }
//     return res.data as ResponseData<T>;
//   } catch (error) {
//     const axiosError = error as AxiosError;
//     console.error('httpService:', axiosError.message);
//     return null;
//   }
// };

const http = {
  get: <T>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> => client.get(url, config),
  post: <T>(url: string, data?: AxiosRequestConfig['data'], config?: AxiosRequestConfig): Promise<AxiosResponse<T>> =>
    client.post(url, data, config),
};

// const httpGET = <T>(url: string, config?: AxiosRequestConfig) =>
//   http<T>('GET', url, config);

// const httpPOST = <T>(
//   url: string,
//   data?: AxiosRequestConfig['data'],
//   config?: AxiosRequestConfig
// ) => http<T>('POST', url, { data, ...config });

// const httpPUT = <T>(
//   url: string,
//   data?: AxiosRequestConfig['data'],
//   config?: AxiosRequestConfig
// ) => http<T>('PUT', url, { data, ...config });

// const httpDELETE = <T>(
//   url: string,
//   data?: AxiosRequestConfig['data'],
//   config?: AxiosRequestConfig
// ) => http<T>('DELETE', url, { params: data, ...config });

// export { httpGET, httpPOST, httpPUT, httpDELETE };
export default http;
