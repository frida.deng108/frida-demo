import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { UserState } from '@/types';
import { getUsers } from '@/api';

const initialState: UserState = {
  loading: false,
  users: [],
  error: '',
};

export const fetchUsers = createAsyncThunk('users/fetchUsers', async () => {
  const response = await getUsers();
  return response;
});

export const userSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchUsers.pending, state => {
      state.loading = true;
    });
    builder.addCase(fetchUsers.fulfilled, (state, action) => {
      state.loading = false;
      state.users = action.payload.data ?? [];
      state.error = '';
    });
    builder.addCase(fetchUsers.rejected, (state, err) => {
      state.loading = false;
      state.users = [];
      console.log('rejected', err);
      // state.error = err;
    });
  },
});

export const userReducer = userSlice.reducer;

// export const { fetchUsersSuccess, fetchUsersFailure } = userSlice.actions;
