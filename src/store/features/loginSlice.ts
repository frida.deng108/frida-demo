import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { LoginState, LoginParams } from '@/types';
import { login } from '@/api';

const initialState: LoginState = {
  auth: false,
  authToken: '',
};

export const fetchLogin = createAsyncThunk('login/auth', async (params: LoginParams) => {
  const response = await login(params);
  return response?.data;
});

export const loginSlice = createSlice({
  name: 'login',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchLogin.pending, state => {
      state.auth = false;
      state.authToken = '';
    });
    builder.addCase(fetchLogin.fulfilled, (state, action) => {
      state.auth = true;
      state.authToken = action.payload?.authToken ?? '';
    });
    builder.addCase(fetchLogin.rejected, (state, err) => {
      state.auth = false;
      state.authToken = '';
      console.log('rejected', err);
    });
  },
});

export const loginReducer = loginSlice.reducer;
