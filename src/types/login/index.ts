export interface LoginParams {
  account: string;
  password: string;
}

export interface AuthData {
  auth: boolean;
  authToken: string;
}

export type LoginState = AuthData;
