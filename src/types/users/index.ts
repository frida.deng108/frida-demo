export interface User {
  id: string;
  name: string;
  email: string;
  userName: string;
  address: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: {
      lat: string;
      lng: string;
    };
  };
  phone: string;
  website: string;
  company: {
    name: string;
    catchPhrase: string;
    bs: string;
  };
}

export type UserList = Array<User>;

export interface UserState {
  loading: boolean;
  users: UserList;
  error: string;
}
