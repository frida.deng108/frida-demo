import http from '@/utils/request';
import { UserList } from '@/types';

export const getUsers = () => http.get<UserList>('/api/usersData');
