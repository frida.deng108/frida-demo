import http from '@/utils/request';
import { LoginParams, AuthData } from '@/types';

export const login = (params: LoginParams) => http.post<AuthData>('/api/login', params);
