import { RefObject, useEffect } from 'react';

// 定义点击外部的回调函数类型
type ClickOutsideHandler = (event: MouseEvent) => void;

export const useClickOutside = (
  ref: RefObject<HTMLElement>,
  handler: ClickOutsideHandler
) => {
  useEffect(() => {
    const listener = (event: MouseEvent) => {
      if (!ref.current || ref.current.contains(event.target as HTMLElement)) {
        return;
      }
      handler(event);
    };
    document.addEventListener('click', listener, { capture: true });
    return () => {
      document.removeEventListener('click', listener, { capture: true });
    };
  }, [ref, handler]);
};
