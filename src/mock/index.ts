import Mock from 'mockjs';

if (process.env.NODE_ENV === 'development') {
  // 配置 MockJS
  Mock.setup({
    timeout: '200-600',
  });

  // 拦截 Axios 请求并返回 Mock 数据
  Mock.mock('/api/getData', 'get', {
    status: 'success',
    message: '数据获取成功',
    'data|1-10': [
      {
        'id|+1': 1,
        name: '@cname',
        'age|18-60': 1,
        'gender|1': ['男', '女'],
        email: '@EMAIL',
        birthday: '@date("yyyy-MM-dd")',
      },
    ],
  });
  Mock.mock('/api/todo/getData', 'get', {
    status: 'success',
    message: '数据获取成功',
    'data|1-10': [
      {
        'id|+1': 1,
        name: '@cname',
      },
    ],
  });
  Mock.mock('/api/usersData', 'get', {
    status: 'success',
    message: '数据提交成功',
    'data|1-10': [
      {
        'id|+1': 1,
        name: '@cname',
        userName: '@cname',
        username: '@cname',
        email: '@EMAIL',
        address: {
          street: '@county(true)',
          suite: '@ctitle(5, 10)',
          city: '@city(true)',
          zipcode: '@zip',
          geo: {
            lat: '@float(60, 100, 8, 10)',
            lng: '@float(60, 100, 8, 10)',
          },
        },
        phone: '@phone',
        website: '@url',
        company: {
          name: '@ctitle(5, 10)',
          catchPhrase: '@cparagraph(1, 3)',
          bs: '@cword(5, 10)',
        },
      },
    ],
  });
  Mock.mock('/api/login', 'post', {
    status: 'success',
    message: '登陆成功',
    data: {
      auth: true,
      authToken: Math.random().toString(36),
    },
  });
}
