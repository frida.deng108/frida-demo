import { routesConfig as commonRoutes } from './common';
import { useRoutes } from 'react-router-dom';
import {} from 'react-router-dom';
import { routesConfig as userRouters } from './user';

export const getRouters = () => {
  return useRoutes([...commonRoutes, ...userRouters]);
};
