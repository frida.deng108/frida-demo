import { RouteObject, Navigate } from 'react-router-dom';
import LazyWrapper from '@/components/lazy-wrapper';
import { LoginPage } from '@/pages/login';

const routesConfig: RouteObject[] = [
  {
    path: '/',
    element: <Navigate to="/login" />,
  },
  {
    path: '/login',
    element: <LoginPage />,
  },
  {
    path: '/home',
    element: <LazyWrapper path="/home" />,
  },
  {
    path: '*',
    element: <>404 Not Found!</>,
  },
];

export { routesConfig };
