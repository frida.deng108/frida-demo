import { RouteObject } from 'react-router-dom';
import User from '@/pages/users';

const routesConfig: RouteObject[] = [
  {
    path: '/user',
    element: <User />,
  },
];

export { routesConfig };
