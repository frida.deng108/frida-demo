import classes from './home.module.scss';
import { Link } from 'react-router-dom';

const HomePage = () => {
  return (
    <div className={classes['page-home']}>
      <div>这是首页</div>
      <Link to="/user">用户信息</Link>
    </div>
  );
};

export default HomePage;
