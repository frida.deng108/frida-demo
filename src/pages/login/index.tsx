import { FC, FormEvent } from 'react';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { fetchLogin } from '@/store/features';
import { LoginParams } from '@/types';

export const LoginPage: FC = () => {
  const dispatch = useAppDispatch();
  const { auth, authToken } = useAppSelector(state => state.login);
  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const account = (event.currentTarget.elements[0] as HTMLInputElement).value;
    const password = (event.currentTarget.elements[1] as HTMLInputElement)
      .value;
    handleLogin({ account, password });
  };
  const handleLogin = (params: LoginParams) => {
    dispatch(fetchLogin(params));
    console.log(auth, authToken);
  };
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="account">用户名</label>
        <input type="text" id="account" />
      </div>
      <div>
        <label htmlFor="password">密码</label>
        <input type="password" id="password" />
      </div>
      <div>
        <button type="submit">登陆</button>
        {/* <button>登陆</button> */}
      </div>
    </form>
  );
};
