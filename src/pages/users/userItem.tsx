import { FC } from 'react';
import { User } from '@/types';

const UserItem: FC<User> = ({ id, name, email }) => (
  <div key={id}>
    <p>{name}</p>
    <p>{email}</p>
    <hr />
  </div>
);

export default UserItem;
