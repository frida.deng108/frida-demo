import { useEffect, FC } from 'react';
import { useAppSelector, useAppDispatch } from '@/hooks';
import { fetchUsers } from '@/store/features/userSlice';
import UserItem from './userItem';
import { User } from '@/types';

const User: FC = () => {
  const dispatch = useAppDispatch();
  const { loading, users, error } = useAppSelector(state => state.users);

  useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  return (
    <div>
      {loading ? (
        <h2>Loading...</h2>
      ) : error ? (
        <h2>{error}</h2>
      ) : (
        <div>
          <h2>User List</h2>
          <div>
            {users?.map((user: User) => <UserItem key={user.id} {...user} />)}
          </div>
        </div>
      )}
    </div>
  );
};

export default User;
