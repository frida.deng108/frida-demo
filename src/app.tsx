import { getRouters } from './router';
import '@/assets/style/index.scss';

const App = () => {
  // useRoutes() is a custom hook provided by react-router-dom
  // It takes in a route config and returns a React element
  // 文件中使用 useRoutes() 并嵌入到应用中：
  const appRoutesElement = getRouters();
  return appRoutesElement;
};

export default App;
