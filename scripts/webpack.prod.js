const path = require('path');
const { merge } = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin'); // 用于压缩 JS 代码
const CompressionPlugin = require('compression-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'production',
  // 在 Webpack 5 版本中默认就集成了很多优化，更多自定义诉求可以参考：Webpack Optimization：https://webpack.docschina.org/configuration/optimization/ 配置。
  optimization: {
    minimize: true,
    minimizer: [
      // 用于压缩 JS 代码
      new TerserPlugin({
        terserOptions: {
          format: {
            // 去掉注释
            comments: true,
          },
        },
        // 用于缓存压缩结果，下次构建加速
        extractComments: false,
      }),
    ],
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/,
        use: [
          // 用于提取 CSS 代码到单独的文件中
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: {
                auto: true,
                localIdentName: '[hash:base64:8]',
              },
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [['autoprefixer']],
              },
            },
          },
          // {
          //   loader: 'sass-loader',
          //   options: {
          //     // 注入全局样式和变量
          //     additionalData: `@import "${path.resolve(
          //       __dirname,
          //       '../src/styles/variables.scss'
          //     )}";`,
          //   },
          // },
          'sass-loader',
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.less$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [['autoprefixer']],
              },
            },
          },
          'less-loader',
        ],
        include: /node_modules/,
      },
    ],
  },
  plugins: [
    // 用于提取 CSS 代码到单独的文件中
    new MiniCssExtractPlugin({
      filename: 'assets/css/[contenthash:8].css',
    }),
    // 用于 gzip 压缩生成的 JS 文件
    new CompressionPlugin({
      algorithm: 'gzip',
      test: /\.(js|css)$/,
      threshold: 10240,
      minRatio: 0.8,
      deleteOriginalAssets: false,
    }),
  ],
});
