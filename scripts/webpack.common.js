const path = require('path');
const chalk = require('chalk');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const pkgJSON = require('../package.json');

console.log('process.env.NODE_ENV: ', process.env.NODE_ENV);

module.exports = {
  // 入口文件
  entry: path.resolve(__dirname, '../src/index.tsx'),
  // 输出文件
  output: {
    filename: '[name].[hash:8].js',
    path: path.resolve(__dirname, '../dist'),
    // 项目中所有资源的基础路径，一般是 CDN 地址
    publicPath: '/',
    // 清除 dist 目录
    clean: true,
  },
  resolve: {
    // 自动解析文件扩展名
    extensions: ['.ts', '.tsx', '.js'],
    // 配置别名
    alias: {
      '@': path.resolve(__dirname, '../src'),
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    corejs: {
                      version: 3,
                    },
                    useBuiltIns: 'usage', // 按需引入 pollyfill
                  },
                ],
                '@babel/preset-react', // React 环境
              ],
              // 用于处理一些公共的函数，例如：Promise、_extends 等
              // 例如：import "@babel/runtime/helpers/esm/Promise";
              // 避免 polyfill 污染全局变量；避免重复引入 polyfill
              plugins: ['@babel/plugin-transform-runtime'],
            },
          },
          'ts-loader',
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.(jpe?g|png|svg|gif)$/i,
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 25 * 1024, // 25kb
          },
        },
        generator: {
          filename: 'assets/imgs/[name].[hash:8][ext]',
        },
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      // 定义在代码中可以替换的一些常量
      __DEV__: process.env.NODE_ENV === 'development',
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '../public/index.html'),
      favicon: path.resolve(__dirname, '../public/favicon.ico'),
      title: pkgJSON.name,
      meta: {
        description: {
          type: 'description',
          content: pkgJSON.description,
        },
      },
      minify: 'auto', // 压缩 HTML
    }),
    new ProgressBarPlugin({
      format: `  :msg [:bar] ${chalk.green.bold(':percent')} (:elapsed s)`,
    }),
  ],
};
