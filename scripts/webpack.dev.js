const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development', // 开发模式
  devtool: 'eval-cheap-module-source-map', // 开发模式下使用
  devServer: {
    hot: true, // 热更新
    open: true, // 编译完自动打开浏览器
    compress: false, // 关闭gzip压缩
    port: 3002, // 开启端口号
    historyApiFallback: true, // 支持 history 路由重定向到 index.html 文件
    proxy: {
      // 代理
      // "/api": {
      //   // 代理路径
      //   target: "http://localhost:7788",
      //   pathRewrite: { "^/api": "" },
      //   changeOrigin: true,
      // }
    },
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: {
                auto: true,
                localIdentName: '[name]__[local]',
              },
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                // 从 package.json 中读取 browserslist
                // 通过 browserslist 帮助 autoprefixer适配浏览器
                // autoprefixer 用于自动根据兼容需求增加 CSS 属性的前缀
                plugins: [['autoprefixer']],
              },
            },
          },
          // {
          //   loader: 'sass-loader',
          //   options: {
          //     // 注入全局样式和变量
          //     additionalData: `@import "${path.resolve(
          //       __dirname,
          //       '../src/styles/index.scss'
          //     )}";`,
          //   },
          // },
          'sass-loader',
          // {
          //   loader: 'sass-resources-loader',
          //   options: {
          //     resources: [path.resolve(__dirname, '../src/styles/index.scss')]
          //   }
          // }
        ],
        // 排除 node_modules 目录
        exclude: /node_modules/,
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [['autoprefixer']],
              },
            },
          },
          'less-loader',
        ],
        include: /node_modules/,
      },
    ],
  },
  stats: 'errors-only', // Webpack 在编译的时候只输出错误日志
});
