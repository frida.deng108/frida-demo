module.exports = {
  env: {
    browser: true, // Allows browser globals like window and document
    es2021: true, // Allows ES12 globals like Set
    commonjs: true, // Allows CommonJS require()
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
  ],
  // Overrides for specific files
  overrides: [
    {
      env: {
        // Allow Node.js globals like process and __dirname
        node: true,
      },
      // Only run on .eslintrc.js files
      files: ['.eslintrc.{js,cjs}'],
      // Allow ES6 syntax
      parserOptions: {
        sourceType: 'script', // Allows import and export
      },
    },
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint', 'react', 'prettier'],
  rules: {
    'prettier/prettier': 2,
    'no-unused-vars': 'off', // Turn off the base rule, as it can report incorrect errors
    'no-console': 0, // Warn about console statements
    'react/react-in-jsx-scope': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
  },
  ignorePatterns: ['scripts/webpack.*', 'dist/'],
};
