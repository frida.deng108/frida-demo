关于 tsconfig.json 文件的配置解析可以参阅：[《会写 TypeScript 但你真的会 TS 编译配置吗？》](https://mp.weixin.qq.com/s/i1AVCXliehk5cMLPmPueOA)

## 文件

- dist/: 是用于存储打包够的文件
- public/: 是用于存放打包的模板入口 HTML 文件
- src/: 是用于开发人员主要编码的文件夹
- .gitignore: 用于配置 Git 忽略哪些文件或文件夹
- tsconfig.json: TypeScript 的项目配置文件
- package-lock.json: 依赖模块的版本信息，用于保证开发环境一致性
